from django.conf.urls import patterns, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns(
    'upload.views',
    url('^$', 'home'),
    url('^get/$', 'get'),
    url('^upload/$', 'upload'),
    url('^share/(?P<image>[a-zA-Z0-9\.]+)$', 'share'),
    # Examples:
    # url(r'^$', 'rehost.views.home', name='home'),
    # url(r'^rehost/', include('rehost.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
