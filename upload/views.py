from django.shortcuts import render, redirect
from django.contrib import messages
import requests
import random

UPLOAD_DIR = "/home/viktor/Python/rehost/rehost/static/"


def home(request):
    return render(request, 'index.html')


def get(request):
    if not 'url' in request.POST or len(request.POST['url']) == 0:
        messages.add_message(request, messages.ERROR, "No URL specified")
        return redirect('/')

    url = request.POST['url']
    req = requests.get(url, stream=True, verify=False)

    headers = req.headers
    if is_valid(request, headers) is False:
        return redirect('/')

    name = "%s.%s" % (name_gen(), (headers['content-type'].split('/'))[1])

    with open(UPLOAD_DIR + name, "w") as img:
        img.write(req.content)
        return redirect('/share/' + name)


def upload(request):
    if request.method != "POST":
        messages.add_message(request, messages.ERROR, "u wot m8")
        return redirect('/')

    file = request.FILES['file']
    # little cheating to be able to use is_valid
    headers = {
        'content-type': file.content_type,
        'content-length': file.size
    }

    if not is_valid(request, headers):
        return redirect('/')

    name = "%s.%s" % (name_gen(), (file.content_type.split('/'))[1])
    with open(UPLOAD_DIR + name, "wb+") as img:
        for chunk in file.chunks():
            img.write(chunk)
            return redirect("/share/" + name)


def share(request, image):
    return render(request, "share.html", {'image': image})


def is_valid(request, headers):
    content_type = headers['content-type'].split('/')
    if not content_type[0] == "image":
        messages.add_message(request, messages.ERROR, "This is not an image!")
        return False

    size = headers['content-length']
    if int(size) > 5 * 1024 * 1024:
        messages.add_message(request, messages.ERROR, "Image too big!")
        return False

    return True

ASCII_LETTERS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
DIGITS = '0123456789'


def name_gen(size=6, chars=ASCII_LETTERS + DIGITS):
    """ Generates a random string with letters and digits """
    return ''.join(random.choice(chars) for x in range(size))
